'use strict'
const aws = require('aws-sdk')
const fs = require('fs')
const cfExports = require('./cf-outputs')
let userPoolId, userPoolClientId, serviceVars

// allow option for client to store vars in root of their project, rather than use setServiceVars
if (fs.existsSync(process.env.PWD + '/cf-service-vars.json')) {
  serviceVars = require(process.env.PWD + '/cf-service-vars.json')
} else if (fs.existsSync(process.env.PWD + '/../cf-service-vars.json')) { // also try one directory up e.g. of PWD is in a test directory
  serviceVars = require(process.env.PWD + '/../cf-service-vars.json')
}

exports.setServiceVars = function (_serviceVars) {
  serviceVars = _serviceVars
}

exports.confirmUserSignup = async function ({ email }) {
  return _confirmUserSignup({ email })
}

exports.getUser = async function ({ email }) {
  return _getUser({ email })
}

exports.getTestUserToken = async function (emailId, userPoolId = null, userPoolClientId = null) {
  const userCreds = { email: _createEmailAddress(emailId), password: '1234Qwerty!' }

  // try to get user
  // eslint-disable-next-line node/handle-callback-err
  await _getUser({ ...userCreds, userPoolId }).catch(async (error) => {
    // if user not found, create user
    await _signupUser({ name: 'service-test-utils-test-user', ...userCreds, userPoolClientId })

    // confirm user signup
    await _confirmUserSignup({ ...userCreds, userPoolId })
  })

  // login user
  const auth = await _loginUser({ ...userCreds, userPoolClientId })

  return Promise.resolve(auth.AuthenticationResult.IdToken)
}

exports.getExistingUserToken = async function ({ email, password }) {
  // login user
  const auth = await _loginUser({ email, password })

  return Promise.resolve(auth.AuthenticationResult.IdToken)
}

exports.signupConfirmedUser = async function ({ email, password, name }) {
  // eslint-disable-next-line no-useless-catch
  try {
    await _signupUser({ email, password, name })
    await _confirmUserSignup({ email })
  } catch (e) {
    throw (e)
  }
}

exports.createEmailAddress = function (timestamp) {
  return _createEmailAddress(timestamp)
}

function _createEmailAddress (timestamp) {
  return 'success+' + timestamp + '@simulator.amazonses.com' // dummy email that prevents hard bounces which limit cognito
}

async function _loginUser ({ email, password, userPoolClientId = null }) {
  // eslint-disable-next-line no-useless-catch
  try {
    if (!userPoolClientId) {
      userPoolClientId = await _getUserPoolClientId()
    }
    return _getCognitoClient().initiateAuth({
      AuthFlow: 'USER_PASSWORD_AUTH', /* required */
      ClientId: userPoolClientId, /* required */
      AuthParameters: {
        USERNAME: email,
        PASSWORD: password
        /* '<StringType>': ... */
      }
    }).promise()
  } catch (e) {
    throw (e)
  }
}

async function _getUser ({ email, userPoolId = null }) {
  // eslint-disable-next-line no-useless-catch
  try {
    if (!userPoolId) {
      userPoolId = await _getUserPoolId()
    }
    return _getCognitoClient().adminGetUser({
      UserPoolId: userPoolId, /* required */
      Username: email /* required */
    }).promise()
  } catch (e) {
    throw (e)
  }
}

async function _signupUser ({ email, password, name, userPoolClientId = null }) {
  // eslint-disable-next-line no-useless-catch
  try {
    if (!userPoolClientId) {
      userPoolClientId = await _getUserPoolClientId()
    }
    await _getCognitoClient().signUp({
      ClientId: userPoolClientId, /* required */
      Password: password, /* required */
      Username: email, /* required */
      UserAttributes: [
        {
          Name: 'name', /* required */
          Value: name
        }
      ]
    }).promise()
  } catch (e) {
    throw (e)
  }
}

async function _confirmUserSignup ({ email, userPoolId = null }) {
  // eslint-disable-next-line no-useless-catch
  try {
    if (!userPoolId) {
      userPoolId = await _getUserPoolId()
    }
    await _getCognitoClient().adminConfirmSignUp({
      UserPoolId: userPoolId, /* required */
      Username: email /* required */
    }).promise()
  } catch (e) {
    throw e
  }
}

function _getCognitoClient () {
  // looks for credentials either in environment vars, or a default profile in ~/.aws/credentials
  return new aws.CognitoIdentityServiceProvider()
}

async function _getUserPoolId () {
  if (!userPoolId) {
    userPoolId = await cfExports.get(serviceVars, 'SharedDUserPoolId')
  }
  return userPoolId
}

async function _getUserPoolClientId () {
  if (!userPoolClientId) {
    userPoolClientId = await cfExports.get(serviceVars, 'UserInterfacePrimaryUserClientId')
  }
  return userPoolClientId
}
