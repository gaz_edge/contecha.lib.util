'use strict'

const { Lambda } = require('aws-sdk')

async function invokeLambda (event, name, serviceName, async = false) {
  const resp = await _invokeRemote(event, name, serviceName, false)
  return JSON.parse(resp.Payload)
}

async function invokeLambdaAsync (event, name, serviceName) {
  await _invokeRemote(event, name, serviceName, true)
  // since there is no payload, no need to return anything
}

async function callLocal (local, event) {
  const callback = jest.fn()
  await local.execute(event, {}, callback)
  return callback.mock.calls[0][1]
}

async function _invokeRemote (event, name, serviceName, async) {
  const lambda = new Lambda()
  const params = {
    FunctionName: `${serviceName}-${process.env.SLS_STAGE}-${name}`,
    Payload: JSON.stringify(event)
  }

  if (async) params.InvocationType = 'Event'

  return await lambda.invoke(params).promise()
}

module.exports = { callLocal, invokeLambda, invokeLambdaAsync }
