'use strict'

let OLD_ENV

exports.beforeFunctionCall = () => {
  OLD_ENV = process.env
  if (process.env.BITBUCKET_DEPLOYMENT_ENVIRONMENT === 'production') throw new Error('Tests should not be run in production!')
  if (process.env.BITBUCKET_DEPLOYMENT_ENVIRONMENT === 'staging' || process.env.BITBUCKET_DEPLOYMENT_ENVIRONMENT === 'develop') {
    process.env.SLS_STAGE = process.env.BITBUCKET_DEPLOYMENT_ENVIRONMENT
  } else {
    process.env.SLS_STAGE = 'dev'
    process.env.AWS_REGION = 'eu-west-1'
  }
  jest.setTimeout(20000) // useful if async doesnt finish
}

exports.afterFunctionCall = () => {
  process.env = OLD_ENV
}
