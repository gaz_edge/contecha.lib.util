'use strict'

const aws = require('aws-sdk')

exports.get = async function (serviceVars, varName) {
  const stacks = await _getCloudFormationClient().describeStacks({ StackName: serviceVars[varName].StackName.replace('{STAGE}', process.env.SLS_STAGE) }).promise()
  let value
  stacks.Stacks[0].Outputs.forEach((output) => {
    if (output.OutputKey === serviceVars[varName].OutputKey) {
      value = output.OutputValue
    }
  })
  return Promise.resolve(value)
}

function _getCloudFormationClient () {
  // looks for credentials either in environment vars, or a default profile in ~/.aws/credentials
  return new aws.CloudFormation()
}
